#!/bin/python

from datetime import datetime, timedelta, timezone
import gitlab

# Maximum number of MRs to process, longest inactive first (0 = unlimited).
MR_MAX = 0
# How many days of inactivity before sending a reminder.
MR_INACTIVE_LIMIT = 30
# Create a comment to bump the MR? Otherwise just stdout.
CREATE_COMMENT = False

def cutoff_date():
    """Use MR_INACTIVE_LIMIT to generate cutoff date."""
    return (datetime.now(timezone.utc) - timedelta(days=MR_INACTIVE_LIMIT)).isoformat()


def get_mr_configs(merge_request):
    """Return a list of kernel config options mentioned in MR description."""
    configs = []
    for line in merge_request.description.split('\n'):
        if line.strip().startswith('CONFIG_'):
            configs.append(line.strip().rstrip(':'))
    return configs


def mr_has_an_ack(merge_request):
    """Return True if MR has at least one Acked-by label and no Nack label."""
    acked = False
    for label in merge_request.labels:
        if label.startswith('Nacked-by:'):
            return False
        if label.startswith('Acked-by:'):
            acked = True
    return acked


def generate_comment(merge_request, configs):
    """Generate comment text and if CREATE_COMMENT is set, post it."""
    comment = f"This merge request has not been updated in over {MR_INACTIVE_LIMIT} days.\n"
    comment += "Please review this MR's current changes regarding the following configuration option(s):\n\n"
    for config in configs:
        comment += ' ' * 5 + config + '\n'
    for line in comment.split('\n'):
        print(' ' + line)
    if CREATE_COMMENT:
        merge_request.notes.create({'body': comment})
        print(f' COMMENT POSTED FOR MR {merge_request.iid}')


def main():
    """Main loop."""
    cutoff = cutoff_date()
    gl = gitlab.Gitlab.from_config('gitlab.com')
    glp = gl.projects.get("cki-project/kernel-ark")
    for count, mr in enumerate(glp.mergerequests.list(as_list=False, state='opened',
                                                      order_by='updated_at', sort='asc',
                                                      labels='Configuration',
                                                      **{'not':{'labels':'No CCs'}},
                                                      updated_before=cutoff),
                               start=1):
        print(f'\n{count:0>2d}. {mr.updated_at[0:16]} {mr.iid:0>4d}: {mr.title}')
        if mr_has_an_ack(mr):
            print(' This MR has at least one Ack and no Nacks. It may be ready to merge. Skipping...')
            continue
        mr_configs = get_mr_configs(mr)
        if not mr_configs:
            print(' This MR has no config options listed in the description. Skipping...')
            continue
        generate_comment(mr, mr_configs)
        if count == MR_MAX:
            break


if __name__ == '__main__':
    main()
