#!/bin/python

import gitlab


def get_mr_configs(merge_request):
    """Return a list of kernel config options mentioned in MR description."""
    configs = []
    for line in merge_request.description.split('\n'):
        if line.strip().startswith('CONFIG_'):
            configs.append(line.strip().rstrip(':'))
    return configs


def get_cc_lines(text):
    """Return a list of lines in the test that start with 'Cc: '."""
    cc_string = []
    if text:
        for line in text.splitlines():
            if line.startswith('Cc: '):
                cc_string.append(line)
    return cc_string


def main():
    """Main loop."""
    gl = gitlab.Gitlab.from_config('gitlab.com')
    glp = gl.projects.get("cki-project/kernel-ark")
    for count, mr in enumerate(glp.mergerequests.list(as_list=False, state='opened',
                                                      order_by='updated_at', sort='asc',
                                                      labels='No CCs'),
                               start=1):
        print(f'\n{count:0>2d}. {mr.updated_at[0:16]} {mr.iid:0>4d}: {mr.title}')
        description_ccs = get_cc_lines(mr.description)
        if description_ccs:
            ccs = '\n '.join(description_ccs)
            print(f" This MR has Cc lines:\n {ccs}\n")
        else:
            for commit in mr.commits():
                commit_ccs = get_cc_lines(commit.message)
                if commit_ccs:
                    ccs = '\n '.join(commit_ccs)
                    print(f" Commit {commit.short_id} has Cc lines:\n {ccs}\n")



if __name__ == '__main__':
    main()
